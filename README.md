# Proyecto2-HOC

Generalized assignment problem solved with particle swarm optimization.

Para ejecutar el proyecto y poder generar las gráficas, instalar GR para nim, ejecutar:
nimble install https://github.com/brentp/nim-plotly

Entrar a la carpeta proyecto2-hoc/gap y ejecutar el siguiente comando:

  ## nim c -r --verbosity:0 --threads:on --hints:off main.nim acción instancia semilla

  Donde acción puede ser:
  instancia: ejecuta una instancia
  semillas: prueba varias semillas, según lo especifique:
  nim c -r --verbosity:1 --threads:on --hints:off main.nim semillas 3  1200
  Con el comando anterior vamos a ejecutar la instancia 3 y vamos a ejecutar 1200 semillas.
  Los registros de los resultados (si son factibles), se encontrarán en la carpeta
  instancia3/ donde el nombre será el costo que se obtuvo con esa semilla.
  grafica: optimiza y después obtenemos su gráfica que se guarda en grafica.svg :

  para graficar el costo por cada iteración en la instancia 3 con la semilla 74 ejecutamos:
  nim c -r --verbosity:0 --threads:on --hints:off main.nim grafica 3 74

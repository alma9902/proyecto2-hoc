import db_sqlite
import tables
include Models
import strutils

type
  Database* = object
    name* : string
    instancia* :string

proc getNombresTareas(database: Database): seq[string] =
  let db = open("./../resources/"&database.name&database.instancia&".db", "", "", "")
  var nombres: seq[string]
  nombres = @[]
  for x in db.fastRows(sql"SELECT NOMBRE FROM TAREAS"):
    nombres.add(x[0])
  db.close()
  return nombres
#devuelve un diccionario de trabajadores con llave igual a su id
proc getTrabajadores*(database: Database): Table[int, Trabajador ] =
  let db = open("./../resources/"&database.name&database.instancia&".db", "", "", "")
  var trabajadores = initTable[int, Trabajador]()

  for x in db.fastRows(sql"SELECT * FROM trabajadores"):
    var trabajador = Trabajador(
      id: parseInt(x[0]),
      nombre : x[1],
      capacidad : parseFloat(x[2]),
      consumida: 0
    )
    trabajadores[parseInt(x[0])] = trabajador

  db.close()
  return trabajadores

proc getTareas*(database: Database, trabajadores: int): Table[int, seq[int]] =
  let db = open("./../resources/"&database.name&database.instancia&".db", "", "", "")
  var tareas = initTable[int, seq[int]]()
  for i in 1..trabajadores:
    var trabajadores: seq[int]
    for x in db.fastRows(sql"SELECT ID_TRABAJADOR FROM COSTOS WHERE ID_TAREA = ? ORDER BY COSTO DESC", i):
       var id_trabajador = parseInt(x[0])
       trabajadores.add(id_trabajador)
    tareas[i] = trabajadores
  db.close()
  return tareas


#devuelve el resultado de la capacidad que requiera un trabajador para
#hacer una tarea
proc getCapacidad*(database: Database, id_tarea, id_trabajador: int):float =
  let db = open("./../resources/"&database.name&database.instancia&".db", "", "", "")
  var capacidad: float = 0
  var consulta = db.getValue(sql"SELECT CAPACIDAD FROM CAPACIDADES WHERE id_tarea = ? AND id_trabajador = ?", id_tarea, id_trabajador)
  db.close()
  if consulta != "":
    capacidad = parseFloat(consulta)
  return capacidad

proc getCosto*(database: Database, id_tarea, id_trabajador: int):float =
  let db = open("./../resources/"&database.name&database.instancia&".db", "", "", "")
  var costo: float = 0
  var consulta = db.getValue(sql"SELECT COSTO FROM COSTOS WHERE id_tarea = ? AND id_trabajador = ?", id_tarea, id_trabajador)
  db.close()
  if consulta != "":
    costo = parseFloat(consulta)
  return costo

#[
  getInfo(db:Database):
    devuelve una matriz donde las columnas corresponden a los ids de los
    trabajadores  y los renglones a los ids de las tareas.
    La matriz tiene tupla (capacidad,costo)

]#
proc getInfo(db: Database): InfoMatriz =
  #inicializamos matriz
  var info: InfoMatriz
  new(info)
  var capacidad,costo: float
  var no_tareas, no_trabajadores: int
  var matriz: seq[seq[(float, float)]]

  if db.instancia == "2":
    no_tareas = 200
    no_trabajadores = 75
  elif db.instancia == "3":
    no_tareas = 50
    no_trabajadores = 20
  elif db.instancia == "4":
    no_tareas = 100
    no_trabajadores = 40

  for tarea in 1..no_tareas:
    var renglon: seq[(float, float)]
    for trabajador in 1..no_trabajadores:
      capacidad = db.getCapacidad(tarea, trabajador)
      costo = db.getCosto(tarea, trabajador)
      renglon.add((capacidad,costo))
    matriz.add(renglon)

  info.values = matriz
  return info

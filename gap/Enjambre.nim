include Solution
import plotly
import chroma
type
  Enjambre*[particulas: static[int], tareas:static[int]] = ref object
    particulas: array[1..particulas, Particula[tareas]]
    mejor_particula: Particula[tareas]
    #mejor posicion por cada iteración
    historico_mejores: seq[Particula[tareas]]

    #[
      Evalúa todas las partículas del enjambre,
      actualiza sus valores e identifica la
      mejor partícula
    ]#

proc inicializa(enjambre: var Enjambre, tareas: static[int], instancia, seed: int)=
  var mejor_costo: float = 99999999
  var mejor_pos:int = 1
  var mejor_part: Particula[tareas]

  setInstancia(instancia)
  setSeed(seed)
  inicializaInfo()
  for i in 1..len(enjambre.particulas):
    var particula :Particula[tareas]
    new(particula)
    particula.generaSolucion()
    enjambre.particulas[i] = particula

    if particula.costo < mejor_costo:
      mejor_costo = particula.costo
      mejor_pos = i
  mejor_part = enjambre.particulas[mejor_pos]
  enjambre.mejor_particula = mejor_part
  enjambre.historico_mejores.add(mejor_part)

proc evalua(enjambre: var Enjambre) =
  var mejor_ronda = high(BiggestFloat)
  var indice:int = 1
  for i in 1..len(enjambre.particulas):
    #var (costo,penalizacion) =
    enjambre.particulas[i].calculaCosto()
    #var costoTotal = costo + penalizacion
    #enjambre.particulas[i].costo = costoTotal
    #enjambre.particulas[i].penalizacion = penalizacion
    if enjambre.particulas[i].costo < mejor_ronda :
      mejor_ronda = enjambre.particulas[i].costo
      indice = i
  #guarda en historico
  let part = enjambre.particulas[indice]
  enjambre.historico_mejores.add(part)
  #compara con el mejor global
  if part.costo < enjambre.mejor_particula.costo:
    enjambre.mejor_particula = part

proc mover(enjambre: var Enjambre, inercia, c_cognitivo, c_social:float) =
  for i in 1..len(enjambre.particulas):
    enjambre.particulas[i].moverParticula(enjambre.mejor_particula.asignacion, inercia, c_cognitivo, c_social)

proc optimiza(enjambre: var Enjambre, iteraciones:int, inercia_max,inercia_min, c_cognitivo, c_social:float, reducir:bool) =
  for i in 1..iteraciones:
    var inercia:float
    if reducir :
       inercia = inercia_max - inercia_min
       inercia *= ((iteraciones-i) / iteraciones)
       inercia += inercia_min
    else:
      inercia = inercia_max
    enjambre.mover(inercia, c_cognitivo, c_social)
    enjambre.evalua()
  if enjambre.mejor_particula.excedido > 0:
    enjambre.mejor_particula.hacerFactible()

proc graficaOptimizacion(enjambre: var Enjambre, iteraciones:int, inercia_max,inercia_min, c_cognitivo, c_social:float, reducir:bool) =
  var colors:seq[Color] = @[]
  var costo = Trace[float](mode: PlotMode.LinesMarkers, `type`: PlotType.Scatter)
  var size = @[4.float]
  costo.marker =Marker[float](size:size, color: colors)
  costo.xs = @[]
  costo.ys = @[]
  costo.text = @[]

  for i in 1..iteraciones:
    colors.add(Color(r: 0.2, g: 0.9, b:0.2, a:1.0))
    costo.xs.add(float(i))
    costo.text.add("costo,iteración")
    var inercia:float
    if reducir :
       inercia = inercia_max - inercia_min
       inercia *= ((iteraciones-i) / iteraciones)
       inercia += inercia_min
    else:
      inercia = inercia_max
    enjambre.mover(inercia, c_cognitivo, c_social)
    enjambre.evalua()
    costo.ys.add(enjambre.mejor_particula.costo)
  if enjambre.mejor_particula.excedido > 0:
    enjambre.mejor_particula.hacerFactible()
  enjambre.mejor_particula.calculaCosto()
  costo.xs.add(float(iteraciones+1))
  costo.ys.add(enjambre.mejor_particula.costo)
  var layout = Layout(title: "Optimización", width: 1200, height: 400,
                      xaxis: Axis(title:"Costo"),
                      yaxis:Axis(title: "Iteración"), autosize:false)
  var p = Plot[float](layout:layout, traces: @[costo])
  p.show("grafica.svg")

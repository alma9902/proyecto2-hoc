type
  Trabajador* = object
    id* : int
    nombre* : string
    capacidad* : float
    consumida* : float

type
  InfoMatriz* = ref object
    values* : seq[seq[(float,float)]]


proc getCapacidad(info: var InfoMatriz, id_tarea, id_trabajador: int): float =
  var capacidad = info.values[id_tarea-1][id_trabajador-1][0]
  return capacidad

proc getCosto(info: var InfoMatriz, id_tarea, id_trabajador: int): float =
  var costo = info.values[id_tarea-1][id_trabajador-1][1]
  return costo

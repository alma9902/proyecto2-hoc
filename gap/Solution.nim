include Database
import Models
import random
import algorithm
import math

#[
  Una solución va a estar representada por un diccionario donde
  la llave es el id de la tarea y cada tarea tiene una lista
  de trabajadores que las realizan.
]#
type
  Particula*[tareas: static[int]] = ref object
    asignacion: array[1..tareas, int]
    costo: float
    penalizacion:float
    excedido:int
    velocidad: array[1..tareas, float]
    registro: array[1..tareas, int]
    registro_costo : float

#[
  Para calcular el costo de la solución, necesitamos, por un lado
  la capacidad que gasta un trabajador al hacer una tarea j así
  como el costo de esta. Por el otro lado, necesitamos saber cuánta
  energía ha consumido cada trabajador. Por lo que vamos a tener
  dos vars globales: la información en una matriz y el diccionario
  de los trabajadores
]#
var num_instancia: int
var informacion: InfoMatriz
var trabajadores: Table[int, Trabajador]
var tareas : Table[int, seq[int]]
var nombres_tareas: seq[string]
var no_tareas: int
var no_trabajadores:int

proc setInstancia(instancia: int) =
  num_instancia = instancia

proc inicializaInfo()=
  var db = Database(
    name:"gap",
    instancia: $num_instancia
  )
  informacion = db.getInfo()
  trabajadores = db.getTrabajadores()
  no_tareas = len(informacion.values)
  no_trabajadores = len(informacion.values[0])
  tareas = db.getTareas(no_tareas)
  nombres_tareas = db.getNombresTareas()

#[
  calculaCosto():
  suma el costo que se tiene cuando un trabajador hace la tarea i.
  Vamos a usar una función de penalización que cuenta todas las unidades
  de capacidad extra y luego la multiplicaremos por \alpha = 500
  ]#

proc calculaCosto(solucion: var Particula) = #(float,float) =
  var costoTotal, penalizacion:float
  var cont : int
  costoTotal = 0
  penalizacion = 0


  for tarea in 1..len(solucion.asignacion):
    var trabajador = solucion.asignacion[tarea]
    var costo = informacion.getCosto(tarea, trabajador)
    var capacidad = informacion.getCapacidad(tarea, trabajador)
    trabajadores[trabajador].consumida += capacidad
    costoTotal += costo

  #Ahora vamos a ver cuál es el deficit de energía en los trabajadores

  for trabajador in 1..len(trabajadores):
    var capacidad = trabajadores[trabajador].capacidad
    var consumida = trabajadores[trabajador].consumida
    if consumida > capacidad:
      cont += 1
      penalizacion += consumida - capacidad
    trabajadores[trabajador].consumida = 0

  solucion.excedido = cont
  #si la penalizacion es positiva entonces la solucion es no factible
  if penalizacion <= 0 and cont == 0:
    echo "============== ¡SOLUCION FACTIBLE! :D  ==============================="
    penalizacion = 0
    cont = 1
  else:
    echo "NO ES SOLUCION FACTIBLE "
    penalizacion *= 500
    cont *= 500
    costoTotal *= float(cont)

  echo "TRABAJADORES EXCEDIDOS : ", solucion.excedido
  echo "COSTO SIN PENALIZACION: ", costoTotal / float(cont)
  echo "COSTO TOTAL :", costoTotal+penalizacion
  echo "**************"
  costoTotal += penalizacion
  #reviso el mejor de cada partícula
  if solucion.registro_costo > costoTotal:
    let asignacion = solucion.asignacion
    solucion.registro = asignacion
    solucion.registro_costo = costoTotal

  solucion.costo = costoTotal
  solucion.penalizacion = penalizacion

proc setSeed(seed: int)=
  randomize(seed)

#[
  Genera la velocidad, este es un vector del tamaño del
  número de tareas. Cada valor va a ser una probabilidad
  con distribución uniforme.
]#
proc generaVectorVelocidad(solucion: var Particula) =
  for i in 1..len(solucion.velocidad):
    solucion.velocidad[i] = rand(1.0)

#[
  initSolucion():Solucion
  -Asigna de manera aleatoria el id del trabajador que realizará
  la tarea i.
  - Calcula el costo de la solución
]#
proc generaSolucion(solucion : var Particula) =
  for tarea in 1..no_tareas:
    var trabajador = rand(1..no_trabajadores)
    solucion.asignacion[tarea] = trabajador
  solucion.calculaCosto()
  solucion.generaVectorVelocidad()
  solucion.registro = solucion.asignacion
  solucion.registro_costo = solucion.costo


proc encuentraPosicion(tarea, trabajador:int): int =
  var trabajadores = tareas[tarea]
  for i in 0..<len(trabajadores):
    if trabajadores[i] == trabajador:
      return i
  return -1

proc actualizaVelocidad(particula: var Particula, inercia, c_cognitivo, c_social: float, mejor_global: openArray[int]) =
  #multiplicamos velocidad actual por el coeficiente de inercia
  for i in 1..len(particula.velocidad):
    var nueva:float = particula.velocidad[i] * inercia
    #resta de las posiciones de los trabajadores en la lista con la
    #posición i-ésima  = diferencia de posiciones
    var pos_registro = encuentraPosicion(i, particula.registro[i])
    var pos_actual = encuentraPosicion(i, particula.asignacion[i])
    var pos_global = encuentraPosicion(i, mejor_global[i-1])
    nueva +=  c_cognitivo * rand(1.0) * float(pos_registro - pos_actual)
    nueva += c_social * rand(1.0) * float(pos_global - pos_actual)
    particula.velocidad[i] = nueva

proc actualizaPosicion(particula: var Particula) =
  for i in 1..len(particula.asignacion):
    #busco posición del trabajador que está asignado en la lista de los
    #trabajadores en la i-ésima tarea pos_actual
    #checo la velocidad[i]  velocidad
    #índice_nueva_ posición = ceil(velocidad + pos_actual)
    #nueva_posición = tareas[tarea].[indice_nueva_posicion]
    var pos = encuentraPosicion(i,particula.asignacion[i])
    var velocidad = particula.velocidad[i]
    var indice = int(round(float(pos) + velocidad))
    if indice >= no_trabajadores :
      indice = no_trabajadores-1
    if indice < 0 :
      indice = 0
    var trabajador = tareas[i][indice]
    particula.asignacion[i] = trabajador

#[
  Mover una partícula implica actualizar su velocidad y posición
]#
proc moverParticula(particula : var Particula, mejor_global: openArray[int], inercia, c_cognitivo, c_social: float)=
  particula.actualizaVelocidad(inercia, c_cognitivo, c_social, mejor_global )
  particula.actualizaPosicion()

proc hacerFactible(particula: var Particula) =
  var relacion = initTable[int, seq[int]]()
  var tareas_para_cambiar : seq[int]
  var trabajadores_cambiar : seq[int]

  for tarea in 1..len(particula.asignacion):
    var trabajador = particula.asignacion[tarea]
    var capacidad = informacion.getCapacidad(tarea, trabajador)
    trabajadores[trabajador].consumida += capacidad

    if trabajadores[trabajador].capacidad < trabajadores[trabajador].consumida:
      tareas_para_cambiar.add(tarea)
      trabajadores_cambiar.add(trabajador)

  for i in low(trabajadores_cambiar)..high(trabajadores_cambiar):
    if not relacion.hasKey(trabajadores_cambiar[i]):
      relacion[trabajadores_cambiar[i]] = @[]
    relacion[trabajadores_cambiar[i]].add(tareas_para_cambiar[i])

  for trabajador, values in relacion:
    for t in values:
      #voy a asignar cada tarea a otro trabajador que sí le quepa
      #y voy a actualizar la energía consumida de ese trabajador
      var c_por_tarea = informacion.getCapacidad(t, trabajador)
      var candidatos = tareas[t]
      #como voy a empezar a buscar desde los candidatos que cobren más barato
      candidatos.reverse()
      for candidato in candidatos:
        #checo la capacidad que requiere la tarea con este candidato y checho la energía consumida del candidatos
        var requerida = informacion.getCapacidad(t, candidato)
        var consumida = trabajadores[candidato].consumida
        if requerida + consumida <= trabajadores[candidato].capacidad:
          #asignamos esa tarea a ese trabajador y actualizamos consumida del trabajador que antes la tenía y el actual
          particula.asignacion[t] = candidato
          trabajadores[candidato].consumida = requerida + consumida
          var c_actualizada = trabajadores[trabajador].consumida  - c_por_tarea
          trabajadores[trabajador].consumida = c_actualizada
          break
  for trabajador in 1..len(trabajadores):
    trabajadores[trabajador].consumida = 0

proc getAsignacion(particula: var Particula): Table[int, seq[int]] =
  var asignacion = initTable[int, seq[int]]()

  for tarea in 1..no_tareas:
    var trabajador = particula.asignacion[tarea]
    if not asignacion.hasKey(trabajador):
      asignacion[trabajador] = @[]
    asignacion[trabajador].add(tarea)
  return asignacion

proc imprimeResultado(particula: var Particula, seed:int)=
  var costo: int
  var archivo:string
  var resultado:string
  var asignacion = particula.getAsignacion()
  costo = int(particula.costo)
  if no_tareas == 200:
    archivo = "./../instancia2/"
  elif no_tareas == 50:
    archivo = "./../instancia3/"
  else:
    archivo = "./../instancia4/"
  archivo = archivo & "cost" & intToStr(costo) & ".txt"
  resultado = "SEMILLA : " & intToStr(seed) & "\n"
  resultado = resultado & "CON COSTO : " & intToStr(costo) & "\n \n"

  for trabajador, values in asignacion:
    var r: string = "El trabajador:  "
    r = r & trabajadores[trabajador].nombre
    r = r & " con Id: "
    r = r & intToStr(trabajador)
    r = r & " tiene las siguientes tareas asignadas : \n"
    var ids: string = "[ "
    var nombres: string = "[ "
    for i in low(values)..high(values):
      ids = ids & intToStr(values[i])
      nombres = nombres & nombres_tareas[values[i]-1]
      if i != high(values):
        ids = ids & ", "
        nombres = nombres & ", \n"
    ids = ids & " ] \n"
    nombres = nombres & " ] \n"
    r = r & ids & nombres
    resultado = resultado & r

  writeFile(archivo, resultado)

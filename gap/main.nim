include Enjambre
import os
import strutils

proc semillasInstancia2(cantidad: int) =
  var enjambre: Enjambre[300,200]
  new (enjambre)
  for i in 1..cantidad:
    echo "PROBANDO CON SEMILLA : ", i
    enjambre.inicializa(200, 2, i)
    enjambre.optimiza(200,0.2,0.1, 1.5, 2.2, false)
    enjambre.mejor_particula.calculaCosto()
    if enjambre.mejor_particula.penalizacion == 0:
      enjambre.mejor_particula.imprimeResultado(i)
      echo "SOLUCIÓN FACTIBLE, COSTO = ", enjambre.mejor_particula.costo

proc semillasInstancia3(cantidad: int)=
  var enjambre: Enjambre[800,50]
  new (enjambre)
  for i in 1..cantidad:
    echo "PROBANDO CON SEMILLA : ", i
    enjambre.inicializa(50, 3, i)
    enjambre.optimiza(200,0.1,0.1, 1.5, 2, false)
    enjambre.mejor_particula.calculaCosto()
    if enjambre.mejor_particula.penalizacion == 0:
      enjambre.mejor_particula.imprimeResultado(i)
      echo "SOLUCIÓN FACTIBLE, COSTO = ", enjambre.mejor_particula.costo

proc semillasInstancia4(cantidad: int) =
  var enjambre: Enjambre[100,100]
  new (enjambre)
  for i in 1..cantidad:
    echo "PROBANDO CON SEMILLA : ", i
    enjambre.inicializa(100, 4, i)
    enjambre.optimiza(300,0.4, 0.1, 0.8, 1.8, true)
    enjambre.mejor_particula.calculaCosto()
    if enjambre.mejor_particula.penalizacion == 0:
      enjambre.mejor_particula.imprimeResultado(i)
      echo "SOLUCIÓN FACTIBLE, COSTO = ", enjambre.mejor_particula.costo

proc ejecutarInstancia2(seed: int) =
  var enjambre: Enjambre[300,200]
  new (enjambre)
  enjambre.inicializa(200, 2, seed)
  enjambre.optimiza(200,0.2,0.1, 1.5, 2.2, false)
  enjambre.mejor_particula.calculaCosto()
  if enjambre.mejor_particula.penalizacion == 0:
    enjambre.mejor_particula.imprimeResultado(seed)
    echo "SOLUCIÓN FACTIBLE, COSTO = ", enjambre.mejor_particula.costo

proc ejecutarInstancia3(seed: int) =
  var enjambre: Enjambre[800,50]
  new (enjambre)
  enjambre.inicializa(50, 3, seed)
  enjambre.optimiza(200,0.1,0.1, 1.5, 2, false)
  enjambre.mejor_particula.calculaCosto()
  if enjambre.mejor_particula.penalizacion == 0:
    enjambre.mejor_particula.imprimeResultado(seed)
    echo "SOLUCIÓN FACTIBLE, COSTO = ", enjambre.mejor_particula.costo

proc ejecutarInstancia4(seed: int) =
  var enjambre: Enjambre[100,100]
  new (enjambre)
  enjambre.inicializa(100, 4, seed)
  enjambre.optimiza(300,0.4, 0.1, 0.8, 1.8, true)
  enjambre.mejor_particula.calculaCosto()
  if enjambre.mejor_particula.penalizacion == 0:
    enjambre.mejor_particula.imprimeResultado(seed)
    echo "SOLUCIÓN FACTIBLE, COSTO = ", enjambre.mejor_particula.costo

proc graficaInstancia2(seed:int) =
  var enjambre: Enjambre[300,200]
  new (enjambre)
  enjambre.inicializa(200, 2, seed)
  enjambre.graficaOptimizacion(200,0.2,0.1, 1.5, 2.2, false)

proc graficaInstancia3(seed:int) =
  var enjambre: Enjambre[800,50]
  new (enjambre)
  enjambre.inicializa(50, 3, seed)
  enjambre.graficaOptimizacion(150,0.1,0.1, 1.5, 2, false)

proc graficaInstancia4(seed:int) =
  var enjambre: Enjambre[100,100]
  new (enjambre)
  enjambre.inicializa(100, 4, seed)
  enjambre.graficaOptimizacion(300,0.4, 0.1, 0.8, 1.8, true)

proc main() =
  #las posibles acciones pueden ser : instancia, semillas, grafica
  var accion = paramStr(1)
  #las posibles instancias pueden ser: 2, 3 o 4
  var instance = parseInt(paramStr(2))
  #la semilla es cualquier número entero
  var seed = parseInt(paramStr(3))
  case accion:
    of "instancia":
      if instance == 2:
        ejecutarInstancia2(seed)
      elif instance == 3:
        ejecutarInstancia3(seed)
      else:
        ejecutarInstancia4(seed)
    of "semillas":
      if instance == 2:
        semillasInstancia2(seed)
      elif instance == 3:
        semillasInstancia3(seed)
      else:
        semillasInstancia4(seed)
    of "grafica":
      echo "Vamos a graficaaaar "
      if instance == 2:
        graficaInstancia2(seed)
      elif instance == 3:
        graficaInstancia3(seed)
      else:
        graficaInstancia4(seed)
    else:
      echo "Por favor, ingresa valores válidos :)"

main()
